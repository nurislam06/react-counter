//@ts-check
import React, { Component } from "react";
import './App.css'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }
  increment = () => {
    this.setState({ count: this.state.count + 1 });
  };
  decrement = () => {
    this.setState({ count: this.state.count - 1 });
  };
  onReset = () => {
    this.setState({ count: (this.state.count = 0) });
  };
  render() {
    return (
      <div className="App">
        <button onClick={this.increment} className="count">+</button>
        <button onClick={this.decrement} className="count">-</button>
        <h3 className="sum">{this.state.count}</h3>
        <button onClick={this.onReset} className="Reset">reset</button> 
      </div>
    );
  }
}

export default App;
